package com.nimbusds.oauth2.sdk;


/**
 * Marker interface for OAuth 2.0 authorisation framework messages.
 *
 * @author Vladimir Dzhuvinov
 */
public interface Message { }
