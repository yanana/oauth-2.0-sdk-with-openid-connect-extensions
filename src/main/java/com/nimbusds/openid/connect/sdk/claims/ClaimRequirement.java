package com.nimbusds.openid.connect.sdk.claims;


/**
 * Enumeration of the claim requirement types.
 *
 * @author Vladimir Dzhuvinov
 */
public enum ClaimRequirement {


	/**
	 * Essential claim.
	 */
	ESSENTIAL,


	/**
	 * Voluntary claim.
	 */
	VOLUNTARY
}